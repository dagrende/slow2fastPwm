/*
 * Convert slow 60Hz pwm signal to fast 1KHz pwm signal.
 * Handles the cases when slow pwm is constant high or constant low.
*/

#include <Arduino.h>
#include <TimerOne.h>

const byte slowPwmIn = 7;
const byte fastPwmOut = 9;

const int fastPwmMaxVal = 1023;

volatile unsigned int dutyCycle256;
volatile boolean timeMeasureEnable = false;

void slowPwmInterrupt();

void setup() {
    pinMode(slowPwmIn, INPUT_PULLUP);
    pinMode(fastPwmOut, OUTPUT);

    attachInterrupt(digitalPinToInterrupt(slowPwmIn), slowPwmInterrupt, CHANGE);


    // set up counter 1 - 16mS to 255, more to call counterOverflowInterrupt
    noInterrupts();  // disable all interrupts
    TCCR3A = 0;
    TCCR3B = 0;
    TCNT3 = 0;
    OCR3A = 1000;            // compare match register reach this in 1/(16000000/1024/270) = 17mS 
    TCCR3B |= (1 << WGM12);   // CTC mode
    TCCR3B |= (1 << CS12) | (1 << CS10);    // 1024 prescaler - count to 256 in 1/(16000000/1024/256) = 16mS
    TIMSK3 |= (1 << OCIE3A);  // enable timer compare interrupt
    interrupts();             // enable all interrupts

    // pin 9 fast pwm out
    Timer1.initialize(1000);  // us period time
    Timer1.pwm(fastPwmOut, 0);

    // Serial.begin(115200);
}

// interrrupt call here on slowPwmIn pin going high or low
void slowPwmInterrupt() {
    if (digitalRead(slowPwmIn) == 1) {
        // rising edge - prepare time measurement
        TCNT3 = 0;  // set counter to 0
        timeMeasureEnable = true;
    } else {
        // falling edge
        if (timeMeasureEnable) {
            dutyCycle256 = TCNT3;  // read counter value
            Timer1.pwm(fastPwmOut, dutyCycle256 * 4); // set fastPwmOut duty cycle to the same as slowPwmIn duty cycle
       }
    }
}

volatile boolean tp2toggle = false;

// interrupt call here on timer reaching 255 that is 16mS after slowPwmPin going high
ISR(TIMER3_COMPA_vect) {          // timer compare interrupt service routine
    // set fast pwm value min or max for slowPwmIn 0 or 1
    boolean slowPin = digitalRead(slowPwmIn);
    dutyCycle256 = slowPin ? 255 : 0;
    Timer1.pwm(fastPwmOut, fastPwmMaxVal * slowPin);
    timeMeasureEnable = false;
}

void loop() {
    delay(200);
    // Serial.println(dutyCycle256);
}
